class CookieConsentManager {
  /**
   * @constructor
   * @param  {Array<string>} categories - List of cookie categories which should be consentable
   * @param  {string} cookiePrefix='consent_cookie_' - Prefix of the consent cookies
   * @param  {int} cookieExpiresDays=365 - Expiration in days for the consent cookies
   */
  constructor(settings) {
    let formTemplate = '';
    let inputs = '';

    if (!settings.title || !settings.description || !Array.isArray(settings.categories) || !settings.categories.length || !settings.labelButtonConsentAll || !settings.labelButtonConsentSelected) {
      throw('Invalid settings');
    }

    this.consentManager = document.createElement('div');
    this.title = settings.title;
    this.description = settings.description;
    this.detailsLabel = settings.detailsLabel;
    this.cookieExpires = settings.cookieExpiresDays || 365;
    this.cookiePrefix = settings.cookiePrefix || 'ae_cookie_consent_';
    this.buttonOrder = settings.buttonOrder || 0;

    settings.categories.forEach(category => {
      inputs += `
        <div class="cookie-consent-manager__checkbox">
          <input type="checkbox" id="cookie-consent-manager-${category.name}" name="${category.name}" value="true" ${category.noCookie ? 'data-no-cookie="true"' : ''} ${category.disabled ? 'disabled' : ''} ${category.default ? 'checked' : ''} />
          <label for="cookie-consent-manager-${category.name}">${category.title}</label>`;

      if(category.subcategories && Array.isArray(category.subcategories) && category.subcategories.length > 0) {
        category.subcategories .forEach(subcategory => {
          inputs += `
            <div class="cookie-consent-manager__sub-checkbox">
              <input type="checkbox" id="cookie-consent-manager-${category.name}-${subcategory.name}" name="${category.name}_${subcategory.name}" value="true" />
              <label for="cookie-consent-manager-${category.name}-${subcategory.name}">${subcategory.title}</label>
            </div>
          `;
        });
      }

      inputs += '</div>';
    });

    const buttonAll = `
      <button class="btn consent-all ${settings.cssClassStringConsentAllButton || ''}">
        ${settings.labelButtonConsentAll}
      </button>
    `;

    const buttonSelected = `
      <button class="btn consent-selected ${settings.cssClassStringConsentSelectedButton || ''}" style="display: none">
        ${settings.labelButtonConsentSelected}
      </button>
    `;

    const buttonConfigure = `
      <button class="btn consent-configure ${settings.cssClassStringConfigureButton || ''}">
        ${settings.labelButtonConfigure}
      </button>
    `;

    formTemplate = `
      <div class="cookie-consent-manager__inner">
        <div class="cookie-consent-manager__headline">
          ${this.title}
        </div>
        <form action="">
          <div class="cookie-consent-manager__buttons">
            ${
              this.buttonOrder === 0
              ? `${buttonAll} ${buttonSelected} ${buttonConfigure}`
              : `${buttonSelected} ${buttonConfigure} ${buttonAll}`
            }
          </div>
          <p class="cookie-consent-manager__description">
            ${this.description}
          </p>
          <div class="cookie-consent-manager__configuration" style="display: none">
            ${inputs}
            <div class="cookie-consent-manager__details">
              <div class="cookie-consent-manager__details__header">
                ${this.detailsLabel}
                <span class="ae-icon ae-icon-arrow-down"></span>
              </div>
              <div class="cookie-consent-manager__details__content">
                ${settings.categories.map(category => `
                  <div class="cookie-consent-manager__details__content__headline">${category.title}</div>
                  <div class="cookie-consent-manager__details__content__text">${category.details}</div>
                  ${category.subcategories && Array.isArray(category.subcategories) ? category.subcategories.map(subcategory => `
                    <div class="cookie-consent-manager__details__sub-content__headline">${subcategory.title}</div>
                    <div class="cookie-consent-manager__details__sub-content__text">${subcategory.details}</div>
                  `).join('') : ''}
                `).join('')}
              </div>
            </div>
          </div>
        </form>
      </div>
    `;

    this.consentManager.className = `cookie-consent-manager ${settings.cssClassString || ''}`;
    this.consentManager.style.display = 'none';
    this.consentManager.insertAdjacentHTML('beforeend', formTemplate);
  
    document.body.appendChild(this.consentManager);
  }

  /**
   * Returns all category checkboxes except the neccessary category checkbox
   * @returns {Array<DomElement>} - Category checkboxes
   */
  _getCategoryCheckboxes() {
    const categories = [];
    const checkboxes = this.consentManager.querySelectorAll('input[type=checkbox]');

    for (let i = 0; i < checkboxes.length; i++) {
      if (!checkboxes[i].getAttribute('data-no-cookie')) {
        categories.push(checkboxes[i]);
      }
    }

    return categories;
  }

  /**
   * Returns a specific category checkbox
   * @param {string} category - The category to return
   * @returns {DomElement} - Category checkbox
   */
  _getCategoryCheckbox(category) {
    let checkToReturn = null;
    this._getCategoryCheckboxes().forEach(categoryCheckbox => {
      if (categoryCheckbox.name === category) {
        checkToReturn = categoryCheckbox;
      }
    });
    
    return checkToReturn;
  }

  /**
   * Checks one specific consent category checkbox
   * @param {string} category - The category name to set the consent for
   * @returns {void}
   */
  _setConsent(category) {
    this._getCategoryCheckbox(category).checked = true;
  }

  /**
   * Checks all consent category checkboxes
   * @returns {void}
   */
  _setConsentAll() {
    this._getCategoryCheckboxes().forEach(categoryCheckbox => categoryCheckbox.checked = true);
  }

  /**
   * Unchecks all consent categories checkboxes to false
   * @returns {void}
   */
  _setConsentNone() {
    this._getCategoryCheckboxes().forEach(categoryCheckbox => categoryCheckbox.checked = false);
  }

  _updateConsent() {
    this._writeAllCookies();
    this._consentGiven();
    this.hide();
  }

  /**
   * The state is valid if a cookie exists for each category
   * @returns {boolean} - Is the cookie state valid
   */
  _stateIsValid() {
    let valid = true;

    this._getCategoryCheckboxes().forEach(categoryCheckbox => {
      if (this._readCookie(categoryCheckbox.name) === null) {
        valid = false;
      }
    });

    return valid;
  }

  /**
   * Writes the given cookie
   * @param {DomElement} categoryCheckbox - The checkbox to write the cookie for
   * @returns {void}
   */
  _writeCookie(categoryCheckbox) {
    const currentDate = new Date();
    currentDate.setTime(currentDate.getTime() + (this.cookieExpires * 60 * 60 * 24 * 1000));
    var expires = 'expires=' + currentDate.toUTCString();
    document.cookie = this.cookiePrefix + categoryCheckbox.name + '=' + categoryCheckbox.checked + ';' + expires + ';path=/';
  };

  /**
   * Writes the cookie for each category checkbox based on the checkbox state
   * @returns {void}
   */
  _writeAllCookies() {
    this._getCategoryCheckboxes().forEach(categoryCheckbox => {
      this._writeCookie(categoryCheckbox);
    });
  };

  /**
   * Reads the cookie for each category and updates the state of the checkbox
   * @returns {void}
   */
  _readAllCookies() {
    this._getCategoryCheckboxes().forEach(categoryCheckbox => {
      const categoryConsent = this._readCookie(categoryCheckbox.name) === 'true';
      this.consentManager.querySelector(`input[name=${categoryCheckbox.name}]`).checked = categoryConsent;
    });
  }

  /**
   * Returns the value of the cookie specified in the parameter
   * @param {string} category - The category to return the cookie value for
   * @returns {null || string}
   */
  _readCookie(category) {
    const name = `${this.cookiePrefix}${category}=`;
    const decodedCookieString = decodeURIComponent(document.cookie);
    const cookies = decodedCookieString.split(';');

    for (let i = 0; i < cookies.length; i++) {
      let cookie = cookies[i];
      while (cookie.charAt(0) === ' ') {
        cookie = cookie.substring(1);
      }

      if (cookie.indexOf(name) === 0) {
        return cookie.substring(name.length, cookie.length);
      }
    }

    return null;
  };

  /**
   * Dispatches the consent given event on window
   * @returns {null || string}
   */
  _consentGiven() {
    const event = new Event('aeCookieConsentGiven');

    this._readAllCookies();
    this.consentManager.dispatchEvent(event);
  }

  /**
   * Binds the element event click listeners and intially shows the ccm or calls the consentGiven function
   * depending on the current consent state
   */
  init() {
    this.consentManager.querySelector('.consent-selected').addEventListener('click', (event) => {
      event.preventDefault();
      this._updateConsent();
    });
    
    this.consentManager.querySelector('.consent-all').addEventListener('click', (event) => {
      event.preventDefault();
      this._setConsentAll();
      this._updateConsent();
    });

    this.consentManager.querySelector('.consent-configure').addEventListener('click', (event) => {
      event.preventDefault();
      this.consentManager.querySelector('.cookie-consent-manager__configuration').style.display = 'block';
      this.consentManager.querySelector('.consent-selected').style.display = 'inline-block';
      this.consentManager.querySelector('.consent-configure').style.display = 'none';
    });

    this.consentManager.querySelector('.cookie-consent-manager__details__header').addEventListener('click', () => {
      const detailsContainer = this.consentManager.querySelector('.cookie-consent-manager__details__content').parentElement;
      if (detailsContainer.className.indexOf('open') !== -1) {
        detailsContainer.className = detailsContainer.className.replace(/\bopen\b/g, "");
      } else {
        detailsContainer.className += ' open';
      }
    });

    if (!this._stateIsValid()) {
      this.show();
    } else {
      this._consentGiven();
    }
  }

  /**
   * Hides the consent manager window
   * @returns {void}
   */
  hide() {
    this.consentManager.style.display = 'none';
  }

  /**
   * Shows the consent manager window after pulling the consent cookies
   * @returns {void}
   */
  show() {
    this._readAllCookies();
    this.consentManager.style.display = 'block';
  }

  /**
   * Returns if a specific category was consented
   * @param  {string} category - Category to return consent status for
   * @returns {boolean}
   */
  hasConsent(category) {
    return this._stateIsValid() && this._readCookie(category) === 'true';
  }

  /**
   * Returns ture if all of the given categories was consented
   * @param  {string[]} categories - Categories to check for consent
   * @returns {boolean}
   */
  hasConsents(categories) {
    let allConsented = true;
    
    categories.forEach(category => {
      if (!this.hasConsent(category)) {
        allConsented = false;
      }
    });

    return allConsented;
  }

  /**
   * Returns true if at least one of the given cateories was consented
   * @param  {string[]} categories - Categories to check for consent
   * @returns {boolean}
   */
  hasConsentOf(categories) {
    return categories.some(this.hasConsent, this);
  }

  /**
   * Consents to a specific category directly
   * @param  {string} category - Category to consent to
   * @returns {void}
   */
  consentToCategory(category) {
    this._setConsent(category);
    this._writeCookie(this._getCategoryCheckbox(category));
    this._consentGiven();
  }
};