"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var CookieConsentManager = /*#__PURE__*/function () {
  /**
   * @constructor
   * @param  {Array<string>} categories - List of cookie categories which should be consentable
   * @param  {string} cookiePrefix='consent_cookie_' - Prefix of the consent cookies
   * @param  {int} cookieExpiresDays=365 - Expiration in days for the consent cookies
   */
  function CookieConsentManager(settings) {
    _classCallCheck(this, CookieConsentManager);

    var formTemplate = '';
    var inputs = '';

    if (!settings.title || !settings.description || !Array.isArray(settings.categories) || !settings.categories.length || !settings.labelButtonConsentAll || !settings.labelButtonConsentSelected) {
      throw 'Invalid settings';
    }

    this.consentManager = document.createElement('div');
    this.title = settings.title;
    this.description = settings.description;
    this.detailsLabel = settings.detailsLabel;
    this.cookieExpires = settings.cookieExpiresDays || 365;
    this.cookiePrefix = settings.cookiePrefix || 'ae_cookie_consent_';
    this.buttonOrder = settings.buttonOrder || 0;
    settings.categories.forEach(function (category) {
      inputs += "\n        <div class=\"cookie-consent-manager__checkbox\">\n          <input type=\"checkbox\" id=\"cookie-consent-manager-".concat(category.name, "\" name=\"").concat(category.name, "\" value=\"true\" ").concat(category.noCookie ? 'data-no-cookie="true"' : '', " ").concat(category.disabled ? 'disabled' : '', " ").concat(category["default"] ? 'checked' : '', " />\n          <label for=\"cookie-consent-manager-").concat(category.name, "\">").concat(category.title, "</label>");

      if (category.subcategories && Array.isArray(category.subcategories) && category.subcategories.length > 0) {
        category.subcategories.forEach(function (subcategory) {
          inputs += "\n            <div class=\"cookie-consent-manager__sub-checkbox\">\n              <input type=\"checkbox\" id=\"cookie-consent-manager-".concat(category.name, "-").concat(subcategory.name, "\" name=\"").concat(category.name, "_").concat(subcategory.name, "\" value=\"true\" />\n              <label for=\"cookie-consent-manager-").concat(category.name, "-").concat(subcategory.name, "\">").concat(subcategory.title, "</label>\n            </div>\n          ");
        });
      }

      inputs += '</div>';
    });
    var buttonAll = "\n      <button class=\"btn consent-all ".concat(settings.cssClassStringConsentAllButton || '', "\">\n        ").concat(settings.labelButtonConsentAll, "\n      </button>\n    ");
    var buttonSelected = "\n      <button class=\"btn consent-selected ".concat(settings.cssClassStringConsentSelectedButton || '', "\" style=\"display: none\">\n        ").concat(settings.labelButtonConsentSelected, "\n      </button>\n    ");
    var buttonConfigure = "\n      <button class=\"btn consent-configure ".concat(settings.cssClassStringConfigureButton || '', "\">\n        ").concat(settings.labelButtonConfigure, "\n      </button>\n    ");
    formTemplate = "\n      <div class=\"cookie-consent-manager__inner\">\n        <div class=\"cookie-consent-manager__headline\">\n          ".concat(this.title, "\n        </div>\n        <form action=\"\">\n          <div class=\"cookie-consent-manager__buttons\">\n            ").concat(this.buttonOrder === 0 ? "".concat(buttonAll, " ").concat(buttonSelected, " ").concat(buttonConfigure) : "".concat(buttonSelected, " ").concat(buttonConfigure, " ").concat(buttonAll), "\n          </div>\n          <p class=\"cookie-consent-manager__description\">\n            ").concat(this.description, "\n          </p>\n          <div class=\"cookie-consent-manager__configuration\" style=\"display: none\">\n            ").concat(inputs, "\n            <div class=\"cookie-consent-manager__details\">\n              <div class=\"cookie-consent-manager__details__header\">\n                ").concat(this.detailsLabel, "\n                <span class=\"ae-icon ae-icon-arrow-down\"></span>\n              </div>\n              <div class=\"cookie-consent-manager__details__content\">\n                ").concat(settings.categories.map(function (category) {
      return "\n                  <div class=\"cookie-consent-manager__details__content__headline\">".concat(category.title, "</div>\n                  <div class=\"cookie-consent-manager__details__content__text\">").concat(category.details, "</div>\n                  ").concat(category.subcategories && Array.isArray(category.subcategories) ? category.subcategories.map(function (subcategory) {
        return "\n                    <div class=\"cookie-consent-manager__details__sub-content__headline\">".concat(subcategory.title, "</div>\n                    <div class=\"cookie-consent-manager__details__sub-content__text\">").concat(subcategory.details, "</div>\n                  ");
      }).join('') : '', "\n                ");
    }).join(''), "\n              </div>\n            </div>\n          </div>\n        </form>\n      </div>\n    ");
    this.consentManager.className = "cookie-consent-manager ".concat(settings.cssClassString || '');
    this.consentManager.style.display = 'none';
    this.consentManager.insertAdjacentHTML('beforeend', formTemplate);
    document.body.appendChild(this.consentManager);
  }
  /**
   * Returns all category checkboxes except the neccessary category checkbox
   * @returns {Array<DomElement>} - Category checkboxes
   */


  _createClass(CookieConsentManager, [{
    key: "_getCategoryCheckboxes",
    value: function _getCategoryCheckboxes() {
      var categories = [];
      var checkboxes = this.consentManager.querySelectorAll('input[type=checkbox]');

      for (var i = 0; i < checkboxes.length; i++) {
        if (!checkboxes[i].getAttribute('data-no-cookie')) {
          categories.push(checkboxes[i]);
        }
      }

      return categories;
    }
    /**
     * Returns a specific category checkbox
     * @param {string} category - The category to return
     * @returns {DomElement} - Category checkbox
     */

  }, {
    key: "_getCategoryCheckbox",
    value: function _getCategoryCheckbox(category) {
      var checkToReturn = null;

      this._getCategoryCheckboxes().forEach(function (categoryCheckbox) {
        if (categoryCheckbox.name === category) {
          checkToReturn = categoryCheckbox;
        }
      });

      return checkToReturn;
    }
    /**
     * Checks one specific consent category checkbox
     * @param {string} category - The category name to set the consent for
     * @returns {void}
     */

  }, {
    key: "_setConsent",
    value: function _setConsent(category) {
      this._getCategoryCheckbox(category).checked = true;
    }
    /**
     * Checks all consent category checkboxes
     * @returns {void}
     */

  }, {
    key: "_setConsentAll",
    value: function _setConsentAll() {
      this._getCategoryCheckboxes().forEach(function (categoryCheckbox) {
        return categoryCheckbox.checked = true;
      });
    }
    /**
     * Unchecks all consent categories checkboxes to false
     * @returns {void}
     */

  }, {
    key: "_setConsentNone",
    value: function _setConsentNone() {
      this._getCategoryCheckboxes().forEach(function (categoryCheckbox) {
        return categoryCheckbox.checked = false;
      });
    }
  }, {
    key: "_updateConsent",
    value: function _updateConsent() {
      this._writeAllCookies();

      this._consentGiven();

      this.hide();
    }
    /**
     * The state is valid if a cookie exists for each category
     * @returns {boolean} - Is the cookie state valid
     */

  }, {
    key: "_stateIsValid",
    value: function _stateIsValid() {
      var _this = this;

      var valid = true;

      this._getCategoryCheckboxes().forEach(function (categoryCheckbox) {
        if (_this._readCookie(categoryCheckbox.name) === null) {
          valid = false;
        }
      });

      return valid;
    }
    /**
     * Writes the given cookie
     * @param {DomElement} categoryCheckbox - The checkbox to write the cookie for
     * @returns {void}
     */

  }, {
    key: "_writeCookie",
    value: function _writeCookie(categoryCheckbox) {
      var currentDate = new Date();
      currentDate.setTime(currentDate.getTime() + this.cookieExpires * 60 * 60 * 24 * 1000);
      var expires = 'expires=' + currentDate.toUTCString();
      document.cookie = this.cookiePrefix + categoryCheckbox.name + '=' + categoryCheckbox.checked + ';' + expires + ';path=/';
    }
  }, {
    key: "_writeAllCookies",
    value:
    /**
     * Writes the cookie for each category checkbox based on the checkbox state
     * @returns {void}
     */
    function _writeAllCookies() {
      var _this2 = this;

      this._getCategoryCheckboxes().forEach(function (categoryCheckbox) {
        _this2._writeCookie(categoryCheckbox);
      });
    }
  }, {
    key: "_readAllCookies",
    value:
    /**
     * Reads the cookie for each category and updates the state of the checkbox
     * @returns {void}
     */
    function _readAllCookies() {
      var _this3 = this;

      this._getCategoryCheckboxes().forEach(function (categoryCheckbox) {
        var categoryConsent = _this3._readCookie(categoryCheckbox.name) === 'true';
        _this3.consentManager.querySelector("input[name=".concat(categoryCheckbox.name, "]")).checked = categoryConsent;
      });
    }
    /**
     * Returns the value of the cookie specified in the parameter
     * @param {string} category - The category to return the cookie value for
     * @returns {null || string}
     */

  }, {
    key: "_readCookie",
    value: function _readCookie(category) {
      var name = "".concat(this.cookiePrefix).concat(category, "=");
      var decodedCookieString = decodeURIComponent(document.cookie);
      var cookies = decodedCookieString.split(';');

      for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];

        while (cookie.charAt(0) === ' ') {
          cookie = cookie.substring(1);
        }

        if (cookie.indexOf(name) === 0) {
          return cookie.substring(name.length, cookie.length);
        }
      }

      return null;
    }
  }, {
    key: "_consentGiven",
    value:
    /**
     * Dispatches the consent given event on window
     * @returns {null || string}
     */
    function _consentGiven() {
      var event = new Event('aeCookieConsentGiven');

      this._readAllCookies();

      this.consentManager.dispatchEvent(event);
    }
    /**
     * Binds the element event click listeners and intially shows the ccm or calls the consentGiven function
     * depending on the current consent state
     */

  }, {
    key: "init",
    value: function init() {
      var _this4 = this;

      this.consentManager.querySelector('.consent-selected').addEventListener('click', function (event) {
        event.preventDefault();

        _this4._updateConsent();
      });
      this.consentManager.querySelector('.consent-all').addEventListener('click', function (event) {
        event.preventDefault();

        _this4._setConsentAll();

        _this4._updateConsent();
      });
      this.consentManager.querySelector('.consent-configure').addEventListener('click', function (event) {
        event.preventDefault();
        _this4.consentManager.querySelector('.cookie-consent-manager__configuration').style.display = 'block';
        _this4.consentManager.querySelector('.consent-selected').style.display = 'inline-block';
        _this4.consentManager.querySelector('.consent-configure').style.display = 'none';
      });
      this.consentManager.querySelector('.cookie-consent-manager__details__header').addEventListener('click', function () {
        var detailsContainer = _this4.consentManager.querySelector('.cookie-consent-manager__details__content').parentElement;

        if (detailsContainer.className.indexOf('open') !== -1) {
          detailsContainer.className = detailsContainer.className.replace(/\bopen\b/g, "");
        } else {
          detailsContainer.className += ' open';
        }
      });

      if (!this._stateIsValid()) {
        this.show();
      } else {
        this._consentGiven();
      }
    }
    /**
     * Hides the consent manager window
     * @returns {void}
     */

  }, {
    key: "hide",
    value: function hide() {
      this.consentManager.style.display = 'none';
    }
    /**
     * Shows the consent manager window after pulling the consent cookies
     * @returns {void}
     */

  }, {
    key: "show",
    value: function show() {
      this._readAllCookies();

      this.consentManager.style.display = 'block';
    }
    /**
     * Returns if a specific category was consented
     * @param  {string} category - Category to return consent status for
     * @returns {boolean}
     */

  }, {
    key: "hasConsent",
    value: function hasConsent(category) {
      return this._stateIsValid() && this._readCookie(category) === 'true';
    }
    /**
     * Returns ture if all of the given categories was consented
     * @param  {string[]} categories - Categories to check for consent
     * @returns {boolean}
     */

  }, {
    key: "hasConsents",
    value: function hasConsents(categories) {
      var _this5 = this;

      var allConsented = true;
      categories.forEach(function (category) {
        if (!_this5.hasConsent(category)) {
          allConsented = false;
        }
      });
      return allConsented;
    }
    /**
     * Returns true if at least one of the given cateories was consented
     * @param  {string[]} categories - Categories to check for consent
     * @returns {boolean}
     */

  }, {
    key: "hasConsentOf",
    value: function hasConsentOf(categories) {
      return categories.some(this.hasConsent, this);
    }
    /**
     * Consents to a specific category directly
     * @param  {string} category - Category to consent to
     * @returns {void}
     */

  }, {
    key: "consentToCategory",
    value: function consentToCategory(category) {
      this._setConsent(category);

      this._writeCookie(this._getCategoryCheckbox(category));

      this._consentGiven();
    }
  }]);

  return CookieConsentManager;
}();

;